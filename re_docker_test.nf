#!/usr/bin/env nextflow

nextflow.enable.dsl = 2

if (workflow.containerEngine == 'singularity' || workflow.containerEngine == 'docker') {
    re_path = "/repex_tarean/"
}
else {
    re_path = "/home/lexa/git/repex_tarean/"
}

params.test_data = "${baseDir}/data/test_data/LAS_paired_10k.fas"

process RUN_RE {
    publishDir "${baseDir}/test_out", mode: 'copy', enabled: true

    container "kavonrtep/repeatexplorer:2.3.8"

    input:
    path test_data

    output:
    path "RE/"

    script:
    """
    ${re_path}seqclust -p -v RE ${test_data}
    """
}

params.test_nester = "${baseDir}/data/test_data/151kb_adh1_bothPrimes.fasta"

process RUN_TEG_NESTER {
    publishDir "${baseDir}/test_out", mode: 'copy', enabled: true

    container "tegreedy/nested:2.0.0"

    input:
    path reference

    output:
    path "nester_out/"

    script:
    """
    mkdir nester_out
    nested-nester -d nester_out -n 4 ${reference}
    """
}

/*
 * main script flow
 */

workflow {
    RUN_RE(params.test_data)
    RUN_TEG_NESTER(params.test_nester)
}
