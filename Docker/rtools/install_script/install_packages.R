install.packages("pak", repos = sprintf("https://r-lib.github.io/p/pak/stable/%s/%s/%s", .Platform$pkgType, R.Version()$os, R.Version()$arch))

# in case there is some issues with installing packages, comment line 1 and uncomment line 4 
# install.packages("pak", repos = sprintf("https://r-lib.github.io/p/pak/devel/%s/%s/%s", .Platform$pkgType, R.Version()$os, R.Version()$arch))

pak::pkg_install("GenomicRanges")
pak::pkg_install("Rsamtools")
pak::pkg_install("circlize")
pak::pkg_install("dplyr")
pak::pkg_install("ggplot2")
pak::pkg_install("gplots")
pak::pkg_install("karyoploteR")
pak::pkg_install("ragg")
pak::pkg_install("reshape2")
pak::pkg_install("rtracklayer")
pak::pkg_install("stringr")

pak::pak_cleanup(package_cache = TRUE, metadata_cache = TRUE, force = TRUE)
