# 
# Reads in *.tab files and prepares subtables with contacts over certain distances
# These are saved into *.Rdata files for further use by downstream R scripts
# Tables in text format are published to the output dir
# 
args = commandArgs(trailingOnly=TRUE)
print(args)

tab <- read.table(args[1], sep="\t", header=T, stringsAsFactors=F)

save(tab, file="tab_RE.Rdata")


