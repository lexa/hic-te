library(rtracklayer)

args = commandArgs(trailingOnly=TRUE)

## import the bed file from STDIN
con <- file("stdin")
open(con, blocking=TRUE)
bed.ranges <- import.bed(con)
close(con)

## export as a gff3 file
#con <- file("stdout")
#open(con, type="w", blocking=TRUE)
export.gff3(bed.ranges, args[1])
#close(con)

