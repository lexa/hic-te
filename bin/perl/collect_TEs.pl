#! /usr/bin/perl -w

$header = "";
$oldheader = "";

while(<STDIN>) {
  chop();
  ($sf,$family,$chr,$teid) = split(/\t/);
  $header = "$sf\:\:$family";
  # print "Found $sf $family $chr $teid\n";
  ($te,$id) = split(/-/,$teid);
  # print "TEID=$teid TE=$te ID=$id\n";
  $result = `cat /home/jedlickap/Slyc_newAssembly_SL4.0/SL*/*OUT/data/$chr/TE/$id.fa \| grep -v \"^>\"`;
  if($header ne $oldheader) {
    print ">$header\n";
    $oldheader = $header;
  }
  print $result;
}
