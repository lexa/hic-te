#!/usr/bin/perl -w

# 
# randomize_read_pairing.pl
#

# Read FASTQ file that will have header numbers changed, STDIN has a list of header numbers in order
open(FILE, "$ARGV[0]");
# foreach 4 lines in FASTQ
while($header1 = <FILE>) {
  $seq = <FILE>;
  $header2 = <FILE>;
  $qual = <FILE>;
# get header from the list of headers
  $newheader = <STDIN>;
  chop($newheader);
  # get number from the list of headers
  $newheader =~ m/\.([0-9]+)/;
  $numread = $1;
  # put header and number in place of original ones
  $header1 =~ s/SRR[^ ]+/$newheader/;
  $header1 =~ s/ [0-9]+ / $numread /;
  $header2 =~ s/SRR[^ ]+/$newheader/;
  $header2 =~ s/ [0-9]+ / $numread /;
  
  # print FASTQ with new numbering
  print "$header1$seq$header2$qual";
}

close(FILE);

exit 1;

