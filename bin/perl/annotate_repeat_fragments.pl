#!/usr/bin/env perl

@frag = ();
@dom = ();

$isfrag = 0;
$isdom = 0;
$finished = 0;

#print STDERR "STARTING...\n";

while(not($finished)) {
  #print STDERR ">>MAIN LOOP\n";
  $isfrag = 1;
  if(length($_) > 1) {
    push(@frag,$_);
  }
  while($isfrag) {
    $_=<STDIN>;
    chop();
    #print STDERR "READ FRAG: $_\n";
    if(length($_) < 2) {
      $finished = 1;
      last;
    }
    if(m/fragment/) {
      push(@frag,$_);
    } else {
      $isfrag = 0;
      #print "FRAG: ";
      #print scalar(@frag);
      #print "\n";
      #@frag = ();
    }
  }
  $isdom = 1;
  if(length($_) > 1) {
    push(@dom,$_);
  }
  while(not($finished) & $isdom) {
    $_=<STDIN>;
    chop();
    #print STDERR "READ DOM: $_\n";
    if(length($_)<2) {
      $finished = 1;
      last;
    }
    if(m/polypeptide/) {
      push(@dom,$_);
    } else {
      $isdom = 0;
      #print "DOMS: ";
      #print scalar(@dom);
      #print "\n";
      # == PRINT RESULTS ==
      @annot = ();
      foreach(@dom) {
        m/annot=([^ ;]+)[ ;]/;
        push(@annot,$1);
      }
      $isannotated = 0;
      while(not($isannotated)) {
        $old = $annot[0];
        $problem = 0;
        foreach(@annot) {
          if($_ ne $old) {
            $problem=1;
          }
        }
        if($problem) {
          foreach(@annot) {
            s/::[^:]+$//;
          }
        } else {
          $isannotated = 1;
        }
      }
      $annot[0] =~ m/::([^:]+)$/;
      $annotation = $1;
      $frag[scalar(@frag)-1] =~ m/name.TE ([0-9]+)/;
      $lastTEnum = $1;
      #print STDERR "$lastTEnum\n";
      foreach(@frag) {
        m/name.TE ([0-9]+)/;
        if($1 == $lastTEnum) {
          s/name/annot=$annotation;name/;
        } else {
          s/name/annot="LTR";name/;
        }
        print "$_\n";
      }
      foreach(@dom) {
        m/annot=([^ ;]+)[ ;]/;
        #print "$1\n";
      }
      # == END PRINT RESULTS ==
      $annotation = "LTR";
      @dom = ();
      @frag = ();
    }
  }
}

#print "DOMS: ";
#print scalar(@dom);
#print "\n";
      # == PRINT RESULTS ==
      @annot = ();
      foreach(@dom) {
        m/annot=([^ ;]+)[ ;]/;
        push(@annot,$1);
      }
      $isannotated = 0;
      while(not($isannotated)) {
        $old = $annot[0];
        $problem = 0;
        foreach(@annot) {
          if($_ ne $old) {
            $problem=1;
          }
        }
        if($problem) {
          foreach(@annot) {
            s/::[^:]+$//;
          }
        } else {
          $isannotated = 1;
        }
      }
      $annot[0] =~ m/::([^:]+)$/;
      $annotation = $1;
      $frag[scalar(@frag)-1] =~ m/name.TE ([0-9]+)/;
      $lastTEnum = $1;
      #print STDERR "$lastTEnum\n";
      foreach(@frag) {
        m/name.TE ([0-9]+)/;
        if($1 == $lastTEnum) {
          s/name/annot=$annotation;name/;
        } else {
          s/name/annot="LTR";name/;
        }
        print "$_\n";
      }
      foreach(@dom) {
        m/annot=([^ ;]+)[ ;]/;
        #print "$1\n";
      }
      # == END PRINT RESULTS ==
      

