#!/bin/bash

bedtools shuffle -i Osativa_323_v7.0_fragments_annotated2.gff -g Osativa_323_v7.0.genome > Osativa_323_v7.0_fragments_annotated3_shuffled.gff
#bedtools shuffle -i Osativa_323_v7.0_fragments_annotated2.gff -g Osativa_323_v7.0.genome > Osativa_323_v7.0_fragments_annotated3.gff
bedtools intersect -abam SRR6765292_diachromatic_truncated_paired_uniq_ref.bam -b Osativa_323_v7.0_fragments_annotated2.gff -wb -bed > Osativa_323_v7.0_fragments_toHiC_All.bed
bedtools intersect -abam SRR6765292_diachromatic_truncated_paired_uniq_ref.bam -b Osativa_323_v7.0_fragments_annotated3_shuffled.gff -wb -bed > Osativa_323_v7.0_fragments_toHiC_All_shuffled.bed
cat Osativa_323_v7.0_fragments_toHiC_All.bed | cut -f 4,7,8,13,21 | tr -d , | perl extract_pairs.pl 1 999999999 TRUE | tr -d "\"" | perl ~/git/hic-te/bin/perl/sort_uniq_by_reads.pl > Osativa_323_v7.0_fragments_toHiC_All.tab
cat Osativa_323_v7.0_fragments_toHiC_All_shuffled.bed | cut -f 4,7,8,13,21 | tr -d , | perl extract_pairs.pl 1 999999999 TRUE | tr -d "\"" | perl ~/git/hic-te/bin/perl/sort_uniq_by_reads.pl > Osativa_323_v7.0_fragments_toHiC_All_shuffled.tab

