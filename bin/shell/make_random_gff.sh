#!/bin/bash

# Will take one argument with basename of the reference genome, i.e. Slycopersicum_v4.0
# 
# Seed 4 types of intervals of length 1000 and 10000 and generate 1000 or 100000 of each, labeling them 
bash print_seed_gff.sh 20 1000 > random.gff
bash print_seed_gff.sh 20 10000 >> random.gff
bash print_seed_gff.sh 1000 1000 >> random.gff
bash print_seed_gff.sh 1000 10000 >> random.gff
# Count bp in chromosomes of reference genome
cat $1.fasta | awk '$0 ~ ">" {if (NR > 1) {print c;} c=0;printf substr($0,2,100) "\t"; } $0 !~ ">" {c+=length($0);} END { print c; }' > $1.genome
bedtools shuffle -i random.gff -g $1.genome > $1_random.gff

