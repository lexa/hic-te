#!/bin/bash

for ITER in $(seq 1 1 $1)
do
  echo "chr1	shuffleBed	exon	1	$2	.	+	.	ID=$ITER;Name=random_$2_$1;annot=random_$2_$1"
done
