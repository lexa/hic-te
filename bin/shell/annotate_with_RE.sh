#!/bin/bash
set -e

#reference="Sitalica_312_v2.fa" #choose the reference
#reference="Sbicolor_v3.fasta" #choose the reference
#reference="Zmays_B73_v4.fasta" #choose the reference
#reference="Athaliana_167.fa" #choose the reference
#reference="Slycopersicum_v4.0.fasta" #choose the reference
reference="Osativa_323_v7.0.fa" #choose the reference
contigs="contigs.fasta" #choose the contig file produced by the RE
cluster_table="CLUSTER_TABLE.csv" #choose the cluster table produced by the RE
plantsat="PlantSat.fa"

reference_name="${reference%.*}"
contigs_name="${contigs%.*}"
sam_file=${reference_name}.${contigs_name}
plantsat_file=${reference_name}.${plantsat}

echo ${sam_file}

#check if the bwa index exists for the reference and create it if does not
if [ ! -f ${reference}.bwt ]; then
    echo "Reference index not found!"
    bwa index ${reference}
fi

#map contigs to the reference

#bwa mem -x intractg ${reference} ${contigs} >${sam_file}.sam
java -Xmx18g -cp /usr/share/java/bbmap.jar align2.BBMapPacBio build=1 t=2 overwrite=true minratio=0.60 fastareadlen=2000 minscaf=100 startpad=10000 stoppad=10000 midpad=6000 ref=${reference} in=${contigs} out=${sam_file}.sam mappedonly=t maxindel=256 minhits=2 k=8 ambiguous=all maxsites2=80000 excludefraction=0.001 secondary=t sssr=0.70 maxsites=2000
perl -p -i -e "s/ [^\t]+[\t]/\t/" ${sam_file}.sam
#Add something like:
#perl -p -i -e "s/\t(Chr[^ \t]+) [^\t]+\t/\t\$1\t/" ${sam_file}.sam
#but the "Chr" part is not universal, so we may need to count the number of columns instead
samtools view -bh ${sam_file}.sam -o ${sam_file}.bam
samtools sort --output-fmt BAM -o ${sam_file}.sorted.bam ${sam_file}.bam
mv ${sam_file}.sorted.bam ${sam_file}.bam
bedtools bamtobed -i ${sam_file}.bam >${sam_file}.bed

#map PlantSat to the reference
java -Xmx18g -cp /usr/share/java/bbmap.jar align2.BBMapPacBio build=1 t=2 overwrite=true minratio=0.60 fastareadlen=2000 minscaf=100 startpad=10000 stoppad=10000 midpad=6000 ref=${reference} in=${plantsat} out=${plantsat_file}.sam mappedonly=t maxindel=256 minhits=2 k=8 ambiguous=all maxsites2=80000 excludefraction=0.001 secondary=t sssr=0.70 maxsites=2000
samtools view -bh ${plantsat_file}.sam -o ${plantsat_file}.bam
samtools sort --output-fmt BAM -o ${plantsat_file}.sorted.bam ${plantsat_file}.bam
mv ${plantsat_file}.sorted.bam ${plantsat_file}.bam
bedtools bamtobed -i ${plantsat_file}.bam >${plantsat_file}.bed

#remove the unnecessary alignment files
rm ${sam_file}.sam ${sam_file}.bam
rm ${plantsat_file}.sam ${plantsat_file}.bam

#merge the bed coordinates with the RE annotation
Rscript mergeBedWithAnnotation.R ${sam_file}.bed ${cluster_table}

#filter and convert BED files to GFF
grep "rDNA\|CACTA\|hAT\|LINE\|MuDR\|plastid\|satellite" ${sam_file}.annotated.bed > ${sam_file}.annotated_filtered.bed
cat ${sam_file}.annotated_filtered.bed | Rscript bed2gff.R ${sam_file}.annotated_filtered.gff
perl -p -i -e "s/name=/annot=/" ${sam_file}.annotated_filtered.gff
cat ${plantsat_file}.bed | Rscript bed2gff.R ${plantsat_file}.gff
perl -p -i -e "s/name=/annot=/" ${plantsat_file}.gff


