#!/bin/bash
#written by Matej Lexa
#simplify the output of blast
cut -f 1,2,11 blast.out | perl -p -e "s/REXdb.ID[^\\|]+\\|//" | perl -p -e "s/\|[^\t]+//" | perl -p -e "s/\t[^\t]+.+_/\t/" | perl -p -e "s/(CL[0-9]+)(Contig[0-9]+)/\$1\t\$2/"
