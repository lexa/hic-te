#!/usr/bin/env python
import sys
filename = sys.argv[1] #hitsort.cls from RepeatExplorer is expected. Tuples of lines are present: the first represents the cluster name, the second read names in a numeric form

try:
	with open(filename) as f:
		for line in f:
			cluster_name = line.rstrip().split("\t")[0] #keep only the cluster name (cluster name is followed by a number of reads in a cluster)
			cluster_name = cluster_name.replace(">", "") #remove the fasta sign from the cluster name
			nextline = next(f)
			read_names = nextline.rstrip().split("\t")
			for read in read_names:
				print(read, cluster_name, sep='\t')
except StopIteration:
	print ("ERROR. The input file does not have an even number of lines.")
