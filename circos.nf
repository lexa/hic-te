#! /usr/bin/env nextflow

/* 
 * enables modules 
 */
nextflow.enable.dsl = 2

/*
 * Default pipeline parameters. They can be overriden on the command line eg.
 * given `params.foo` specify on the run command line `--foo some_value`.
 */

params.read_interaction_table = "Slycopersicum_v4.0_fragments_toHiC_All2.tab.toPlot.txt "
params.reference = "/mnt/nas/biodata/hic-te/Slycopersicum_v4.0.fasta"
params.mRNA = "/mnt/nas/biodata/cechova/Hi-C_pipeline/raw_data/oryza_sativa_version7.mRNA.gff3"
params.rm = "/mnt/nas/biodata/cechova/Hi-C_pipeline/raw_data/rice_osa1r7_rm.gff3"

// import modules
include { CREATE_GENOME_FILE; PLOT_CIRCOS } from './modules/plot_circos'

/* 
 * main script flow
 */

workflow {
  read_family_table = Channel.fromPath(params.read_interaction_table)

  println(read_family_table)
  CREATE_GENOME_FILE(params.reference)
  PLOT_CIRCOS(read_family_table)
}
