---
output: 
  posterdown::posterdown_betterport
title: HiC-TE: a Nextflow pipeline for HiC data analysis sheds light on the role of repeats in genome organization
author: Matej Lexa, Monika Cechova, Son Hoang Nguyen, Pavel Jedlicka, Zdenek Kubat, Roman Hobza, Eduard Kejnovsky
---

*ABSTRACT

High-throughput chromosome conformation capture (Hi-C) has become a well established sequencing-based method to detect physical proximity of DNA segments in nuclei, with thousands of Hi-C experiments now available in public repositories (e.g. NCBI Sequence Read Archive (SRA)). We combined this data with increasingly precise public plant reference sequences and tools that characterize the repetitive fraction of genomes, such as Tandem Repeat Finder, PlantSat database, TE-greedy-nester or Repeat Explorer 2. We built a Nextflow pipeline with two main inputs, a SRA sequencing run ID and a corresponding reference genome. The pipeline processes the Hi-C reads, maps and clusters them to identify Hi-C pairs that can be attributed to specific repeat classes, quantifying them within and between repeat families. Results of the analysis are conveniently visualized as heatmaps, circular chromosome plots and exported as data tables. The pipeline is available for use via a gitlab repository. First experiments show biologically important interactions of ribosomal DNA clusters or centromeric repeats that are clearly visible in most plant species. We discovered that LTR retrotransposon families with high interaction rates are often species-specific. This pipeline represents a novel and reproducible way to analyze the role of repetitive elements in the 3D organization of genomes.


