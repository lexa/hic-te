---
main_topsize: 0.2 #percent coverage of the poster
main_bottomsize: 0.1
#ESSENTIALS
title: '**HiC-TE: a Nextflow pipeline for HiC data analysis sheds light on the role of repeats in genome organization**'
author:
  - name: '**Matej Lexa**'
    affil: 1
    main: true
    orcid: 0000-0002-4213-5259
    twitter: matej_lexa
    email: lexa@fi.muni.cz
  - name: Monika Cechova
    affil: 1
  - name: Son Hoang Nguyen
    affil: 2
  - name: Pavel Jedlicka
    affil: 2
  - name: Zdenek Kubat
    affil: 2
  - name: Roman Hobza
    affil: 2
  - name: Eduard Kejnovsky
    affil: 2
affiliation:
  - num: 1
    address: Department of Machine Learning and Data Processing, Faculty of Informatics, Masaryk University, Brno, Czech Republic
  - num: 2
    address: Department of Plant Genomics, Biophysical Institute of the Czech Academy of Sciences, Brno, Czech Republic
main_findings:
  - "**HiC data analysis** with focus on **TEs**: contacts between repeats can be visualized with our **Nextflow pipeline**"
#logoleft_name: https&#58;//raw.githubusercontent.com/brentthorne/posterdown/master/images/betterhexlogo.png
#logoright_name: https&#58;//raw.githubusercontent.com/brentthorne/posterdown/master/images/betterhexlogo.png
#logocenter_name: https&#58;//raw.githubusercontent.com/brentthorne/posterdown/master/images/qr-code-black.png
output: 
  posterdown::posterdown_betterport:
    self_contained: false
    pandoc_args: --mathjax
    number_sections: false
#bibliography: packages.bib
#link-citations: true
---

```{r, include=FALSE}
knitr::opts_chunk$set(echo = FALSE,
                      warning = FALSE,
                      tidy = FALSE,
                      message = FALSE,
                      fig.align = 'center',
                      out.width = "100%")
options(knitr.table.format = "html") 
```

# Abstract

- High-throughput chromosome conformation capture (Hi-C) detects physical proximity of DNA segments
- Hi-C experiments now available in public repositories (e.g. NCBI SRA)
- We combined this data with Tandem Repeat Finder, PlantSat database, TE-greedy-nester, Repeat Explorer 2
- We built a Nextflow pipeline that maps and clusters reads to identify Hi-C pairs in specific repeat classes
- Results are conveniently visualized as heatmaps, circular chromosome plots and exported as data tables
- First experiments show biologically important interactions of ribosomal DNA clusters or centromeric repeats that are clearly visible in most plant species
- LTR retrotransposon families with high interaction rates are often species-specific
- Pipeline represents a novel and reproducible way to analyze the role of repetitive elements in the 3D organization of genomes

&nbsp;  

## Objectives

1. Map repeats in reference (TE-greedy-nester) or reads (Repeat Explorer 2).
2. Find HiC reads that overlap with annotated repeats.
3. Count them by repeat families, bin by distance (local contacts, far contacts and interchromosomal)
4. Generate heatmaps and circos plots showing repeat families.

# Methods

&nbsp;  

![Nextflow pipeline execution](images/nextflow.png){width=1072px}

&nbsp;  

&nbsp;  

![Nextflow pipeline graph](images/graph.png){width=1072px}

&nbsp;  

&nbsp;   

# Results

![Repeat family heatmap](images/heatmap.png){width=1072px}

![Repeat family pair circos plot](images/circos.png){width=1072px}

