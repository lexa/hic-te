#! /usr/bin/env nextflow

/* 
 * enables modules 
 */
nextflow.enable.dsl = 2

/*
 * Default pipeline parameters. They can be overriden on the command line eg.
 * given `params.foo` specify on the run command line `--foo some_value`.
 */

params.SRR_run = "testdata"
params.clustering_threshold = 0.01

params.RE_run = "RE_output_" + params.SRR_run + "_" + params.clustering_threshold
params.TEdatabase = "/mnt/nas/biodata/hic-te/NeumannDB_annotLTRs.fa" //hard path to the blast database


log.info """\
 Hi-C   P I P E L I N E
 ===================================
 SRR_run: ${params.SRR_run}
 clustering_threshold        : ${params.clustering_threshold}
 """

// import modules
include { RUN_RE; FORMAT_READS } from './modules/repeatexplorer'
include { BLAST_CONTIGS; ASSIGN_READS_TO_CLUSTERS; REFORMAT_MATRIX } from './modules/re_to_heatmap'

/* 
 * main script flow
 */

workflow {
  read_pairs_ch = channel.fromFilePairs('/mnt/nas/biodata/cechova/Hi-C_pipeline/raw_data/testdata*_{1,2}.fastq')

  println(read_pairs_ch)
  
  RUN_RE(FORMAT_READS(read_pairs_ch))
  REFORMAT_MATRIX(ASSIGN_READS_TO_CLUSTERS(FORMAT_READS.out.processed_reads,RUN_RE.out.hitsort_cls), RUN_RE.out.clTABLE, BLAST_CONTIGS(RUN_RE.out.contigs_fasta))
}
