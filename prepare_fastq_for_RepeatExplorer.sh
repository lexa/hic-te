#!/bin/bash

# preparing input my way
#use sample to shuffle the fastq files?
reformat.sh in=SRR6765292-HiC.truncated_R1.fastq in2=SRR6765292-HiC.truncated_R2.fastq out=SRR6765292-HiC_truncated.fasta trimreaddescription=f requirebothbad=f minlength=35 forcetrimleft=8 forcetrimmod=-1
cat SRR6765292-HiC_truncated.fasta | fuse_fasta.pl | grep -v "^$" | perl -p -e "if(\$i==1){\$i=2;} else {\$i=1;};s/^(\>[^ \t]+)( .+)$/\$1\/\$i\$2/" > SRR6765292-HiC_truncated.fasta.tmp
mv SRR6765292-HiC_truncated.fasta.tmp SRR6765292-HiC_truncated.fasta
split -d -l 20000000 --additional-suffix=.fa SRR6765292-HiC_truncated.fasta SRR6765292-HiC_truncated_10part
split -d -l 10000000 --additional-suffix=.fa SRR6765292-HiC_truncated.fasta SRR6765292-HiC_truncated_5part
split -d -l 4000000 --additional-suffix=.fa SRR6765292-HiC.truncated.fasta SRR6765292-HiC_truncated_2part
split -d -l 2000000 --additional-suffix=.fa SRR6765292-HiC.truncated.fasta SRR6765292-HiC_truncated_1part
# to prepare a specific FASTA file and generate a header mapping table
cat SRR6765292-HiC_truncated_1part89.fa | fuse_fasta.pl | grep -v "^$" | perl -p -e "if(\$i==1){\$i=2;} else {\$i=1;};s/^(\>[^ \t]+)( .+)$/\$1\/\$i\$2/" | ./prepare_fasta_for_RepeatExplorer.pl SRR6765292-HiC_truncated_1part89

