#! /usr/bin/env nextflow

r_script_path = "${workflow.projectDir}/bin/R"

// Generated reference genome base name
ref_dir = file(params.reference).baseName

save_enabled = params.save_intermediate

use_docker_container = params.singularity_use_docker_container

process PLOT_CIRCOS {

    publishDir "${params.data_dir}/${ref_dir}_${params.sra_run}_png", mode: 'copy', enabled: true

    container "tegreedy/rtools:2.0.0"

    input:
    path read_interaction_table
    path genome_lenght
    path exons_per_window_ch
    path annotated_repeats

    output:
    path "*.png"

    script:
    """
    Rscript "${r_script_path}/plot_family_pairs.R" ${read_interaction_table} ${genome_lenght} ${exons_per_window_ch} ${annotated_repeats}
    """
}

process PREPARE_CIRCOS_FILES {
//Prepare smaller files for the circos to save time (coverage per windows, not per basepair)

    publishDir "${params.data_dir}/annotations", mode: 'copy', enabled: save_enabled

    if (workflow.containerEngine == 'singularity' && !use_docker_container) {
        container "https://depot.galaxyproject.org/singularity/bedtools:2.30.0--hc088bd4_0"
    } else {
        container "quay.io/biocontainers/bedtools:2.30.0--hc088bd4_0"
    }

    input:
    path genome
    path exons_gff3

    output:
    path "${ref_dir}_exons_per_window.bed", emit: exons_per_window_ch

    script:
    """
    bedtools makewindows -g ${genome} -w 1000000 >windows.bed
    bedtools coverage -a windows.bed -b ${exons_gff3} -counts >${ref_dir}_exons_per_window.bed

    """
}
