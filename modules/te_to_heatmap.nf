#!/usr/bin/env nextflow

r_script_path = "${workflow.projectDir}/bin/R"

// Generated reference genome base name
ref_dir = file(params.reference).baseName

use_docker_container = params.singularity_use_docker_container

// This process should go between GENERATE TABLE and the various TE_HEATMAP_* processes.
// It will produce a couple of *.Rd files that these will read into R.
// It should also publish the tables in text format.

process PREPARE_TABLE {
    //Heatmap visualization

    //publishDir "${params.data_dir}/${ref_dir}_${params.sra_run}_png", mode: 'copy', enabled: true

    container "tegreedy/rtools:2.0.0"

    input:
    path table_fragments

    output:
    tuple val(params.sra_run), path("tab*.Rdata"), emit: table_ch

    script:
    """
    Rscript "${r_script_path}"/split_save_tables.R ${table_fragments}
    """
}

process SORT_BAM {
    if (workflow.containerEngine == 'singularity' && !use_docker_container) {
        container "https://depot.galaxyproject.org/singularity/mulled-v2-8186960447c5cb2faa697666dc1e6d919ad23f3e:2200fd433290bb814a952b2fd7cc8013499de840-0"
    } else {
        container "quay.io/biocontainers/mulled-v2-8186960447c5cb2faa697666dc1e6d919ad23f3e:2200fd433290bb814a952b2fd7cc8013499de840-0"
    }
    input:
    path bam_file

    output:
    path "${bam_file}.sorted.bam", emit: sorted_bam
    path "${bam_file}.sorted.bam.bai", emit: sorted_bai
    
    script:
    bam_name = bam_file.baseName
    """
    samtools sort -@ 4 ${bam_file} -o ${bam_file}.sorted.bam
    samtools index ${bam_file}.sorted.bam
    """
}

process PLOT_CHR {
    //Chromomal map of HiC reads

    publishDir "${params.data_dir}/${ref_dir}_${params.sra_run}_png", mode: 'copy', enabled: true

    container "tegreedy/rtools:2.0.0"

    input:
    path genome_file
    path aligned_bam
    path aligned_bai

    output:
    path "*.png"

    script:
    """
    Rscript "${r_script_path}/plotBamCoverage.R" ${genome_file} ${aligned_bam}
    """
}

process PLOT_BARPLOT {
    //Barplot visualization for families

    publishDir "${params.data_dir}/${ref_dir}_${params.sra_run}_png", mode: 'copy', enabled: true

    container "tegreedy/rtools:2.0.0"

    input:
    tuple val(id), path(tab)

    output:
    path "*.png"

    script:
    """
    Rscript "${r_script_path}"/plot_barplot.R ${tab[0]}
    """
}

process TE_HEATMAP_NORM1 {
    //Heatmap visualization

    publishDir "${params.data_dir}/${ref_dir}_${params.sra_run}_png", mode: 'copy', enabled: true

    container "tegreedy/rtools:2.0.0"

    input:
    tuple val(id), path(tab)
    path tab_repeats
    each heatmap_types

    output:
    path "*.png"

    script:
    """
    Rscript "${r_script_path}"/generate_heatmap_reference_joint_probability_norm.R ${tab[0]} ${heatmap_types} ${tab_repeats} ${params.norm_ratio_threshold} ${params.min_fam_pair_count}
    """
}

process TE_HEATMAP_NORM2 {
    //Heatmap visualization

    publishDir "${params.data_dir}/${ref_dir}_${params.sra_run}_png", mode: 'copy', enabled: true

    container "tegreedy/rtools:2.0.0"

    input:
    tuple val(id), path(tab)
    path tab_repeats
    each heatmap_types

    output:
    path "*.png"

    script:
    """
    Rscript "${r_script_path}"/generate_heatmap_reference_label_permutation_norm.R ${tab[0]} ${heatmap_types} ${tab_repeats} "reference" ${params.norm_ratio_threshold} ${params.min_fam_pair_count}
    """
}

process TE_HEATMAP_NORM3 {
    //Heatmap visualization

    publishDir "${params.data_dir}/${ref_dir}_${params.sra_run}_png", mode: 'copy', enabled: true

    container "tegreedy/rtools:2.0.0"

    input:
    tuple val(id), path(tab)
    path tab_shuffled
    path tab_repeats
    each heatmap_types

    output:
    path "*.png"

    script:
    """
    Rscript "${r_script_path}"/generate_heatmap_reference_genome_reshuffling_norm.R ${tab[0]} ${heatmap_types} ${tab_shuffled} ${tab_repeats} ${params.norm_ratio_threshold} ${params.min_fam_pair_count}
    """
}
