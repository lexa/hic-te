#!/usr/bin/env nextflow

save_enabled = params.save_intermediate

use_docker_container = params.singularity_use_docker_container

r_script_path = "${baseDir}/bin/R"
python_script_path = "${baseDir}/bin/python"
shell_script_path = "${baseDir}/bin/shell"

// Generated reference genome base name
ref_dir = file(params.reference).baseName

blast_DB = "${baseDir}/data/NeumannDB_annotLTRs.modified.fa"

process BLAST_CONTIGS {
    //assign reads to the clusters as annotated by RepeatExplorer

    publishDir "${params.data_dir}/re_table", mode: 'copy', enabled: save_enabled

    if (workflow.containerEngine == 'singularity' && !use_docker_container) {
        container "https://depot.galaxyproject.org/singularity/blast:2.12.0--pl5262h3289130_0"
    } else {
        container "quay.io/biocontainers/blast:2.12.0--pl5262h3289130_0"
    }

    input:
    path contigs_fasta

    output:
    path "${params.RE_run}.blastResults.tab"

    script:
    """
    blastn -query ${contigs_fasta} -db "${blast_DB}" -max_target_seqs 1 -outfmt 6 > blast.out
    "${shell_script_path}/formatBlast.sh" > ${params.RE_run}.blastResults.tab
    """
}

process ASSIGN_READS_TO_CLUSTERS {
    //assign reads to the clusters as annotated by RepeatExplorer

    publishDir "${params.data_dir}/re_table", mode: 'copy', enabled: save_enabled

    if (workflow.containerEngine == 'singularity' && !use_docker_container) {
        container "https://depot.galaxyproject.org/singularity/python:3.9--1"
    } else {
        container "quay.io/biocontainers/python:3.9--1"
    }

    input:
    path hitsort_cls

    output:
    path "${params.RE_run}.cls_read_mapping.tab"

    script:
    """
    python3 "${python_script_path}/assignReadsToClusters.py" ${hitsort_cls} > ${params.RE_run}.cls_read_mapping.tab
    """
}

process MERGE_TABLES {
    //assign reads to the clusters as annotated by RepeatExplorer

    publishDir "${params.data_dir}/re_table", mode: 'copy', enabled: save_enabled

    container "tegreedy/rtools:2.0.0"

    input:
    path processed_reads
    path cluster_table

    output:
    path "${params.RE_run}.inClusters.tab", emit: inClusters_ch

    script:
    """
    awk 'NR%4==1' ${processed_reads} | sed s'/^@//g' > fasta_names.txt
    Rscript "${r_script_path}/mergeTables.R" fasta_names.txt ${cluster_table} #writes into read_cluster_association.tab
    cat read_cluster_association.tab | paste -s -d '\t\n' | sed 's/ /\t/g' | sort --temporary-directory="/tmp" -gk1 > "${params.RE_run}.inClusters.tab"
    """
}

process REFORMAT_MATRIX {
    publishDir "${params.data_dir}/table", mode: 'copy', enabled: save_enabled

    container "tegreedy/rtools:2.0.0"

    input:
    path inClusters
    path clTABLE
    path blastResults

    output:
    path "${inClusters}.formatted.tab", emit: formatted_tab_ch

    script:
    """
    #EXAMPLE: SRR6765292_5part00_galaxy.inClusters.tab
    #EXAMPLE: CLUSTER_TABLE.csv
    #EXAMPLE: blast_formatted.out

    Rscript "${r_script_path}/generate_heatmap_reference-free_label_permutation_norm.R" ${inClusters} ${clTABLE} ${blastResults}
    """
}

process SAVE_RE_TABLE {
    
    container "tegreedy/rtools:2.0.0"

    input:
    path formatted_tab

    output:
    path "tab_RE.Rdata", emit: tab_RE_Rdata

    script:
    """
    Rscript "${r_script_path}/save_RE_table.R" ${formatted_tab}
    """
}

process RE_HEATMAP_NORM2 {
    //Heatmap visualization

    publishDir "${params.data_dir}/${ref_dir}_${params.sra_run}_png", mode: 'copy', enabled: true

    container "tegreedy/rtools:2.0.0"

    input:
    path tab
    path tab_repeats

    output:
    path "*.png"

    script:
    """
    Rscript "${r_script_path}/generate_heatmap_reference_label_permutation_norm.R" ${tab} "ALL" ${tab_repeats} "reference_free" ${params.norm_ratio_threshold} ${params.min_fam_pair_count}
    """
}
