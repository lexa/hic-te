#!/usr/bin/env nextflow

perl_script_path = "${workflow.projectDir}/bin/perl"

save_enabled = params.save_intermediate

process GENERATE_TABLE {
    //Generate table
    publishDir "${params.data_dir}/table", mode: 'copy', enabled: save_enabled

    input:
    path bed_file

    output:
    path "${tab_name}.tab", emit: table_ch

    script:
    tab_name = file(bed_file).baseName
    """
    cat ${bed_file} | cut -f 4,7,8,13,21 | tr -d , | perl "${perl_script_path}/extract_pairs.pl" 1 999999999 TRUE | tr -d \"\\"\" | perl "${perl_script_path}/sort_uniq_by_reads.pl" > ${tab_name}.tab
    """
}
