#!/usr/bin/env nextflow

// Generated reference genome base name
ref_dir = file(params.reference).baseName

save_enabled = params.save_intermediate

r_script_path = "${workflow.projectDir}/bin/R"

use_docker_container = params.singularity_use_docker_container

if (workflow.containerEngine == 'singularity' || workflow.containerEngine == 'docker') {
    bbmap_path = "/usr/local/opt/bbmap-38.93-0/current/"
}
else {
    bbmap_path = params.bbmap_java_path
}

process MAP_CONTIGS {
    publishDir "${params.data_dir}/contigs_sam", mode: 'copy', enabled: save_enabled

    if (workflow.containerEngine == 'singularity' && !use_docker_container) {
        container "https://depot.galaxyproject.org/singularity/bbmap:38.93--he522d1c_0"
    } else {
        container "quay.io/biocontainers/bbmap:38.93--he522d1c_0"
    }

    input:
    val reference
    path contigs

    output:
    path "${sam_file}.sam", emit: re_sam_ch

    script:
    sam_file = file(reference).baseName + "_" + contigs.baseName
    """
    java -Xmx18g -cp ${bbmap_path} align2.BBMapPacBio build=1 t=2 overwrite=true minratio=0.60 fastareadlen=2000 minscaf=100 startpad=10000 stoppad=10000 midpad=6000 ref=${reference} in=${contigs} out=${sam_file}.sam mappedonly=t maxindel=256 minhits=2 k=8 ambiguous=all maxsites2=80000 excludefraction=0.001 secondary=t sssr=0.70 maxsites=2000
    """
}

process RE_MODIFY_SAM {

    input:
    path sam_file

    output:
    path "${sam_file}", emit: re_sam_2_ch

    script:
    """
    perl -p -i -e \"s/ [^\t]+[\t]/\t/\" ${sam_file}
    """
}

process BAM_TO_BED {
    publishDir "${params.data_dir}/contigs_bed", mode: 'copy', enabled: save_enabled

    if (workflow.containerEngine == 'singularity' && !use_docker_container) {
        container "https://depot.galaxyproject.org/singularity/mulled-v2-8186960447c5cb2faa697666dc1e6d919ad23f3e:2200fd433290bb814a952b2fd7cc8013499de840-0"
    } else {
        container "quay.io/biocontainers/mulled-v2-8186960447c5cb2faa697666dc1e6d919ad23f3e:2200fd433290bb814a952b2fd7cc8013499de840-0"
    }

    input:
    path sam_file

    output:
    path "${sam_name}.bed" , emit: re_bed_ch

    script:
    sam_name = sam_file.baseName
    """
    samtools view -bhu ${sam_file} | samtools sort -@ 4 - | bedtools bamtobed -i > ${sam_name}.bed
    """
}

process RE_BED_ANNOTATION {
    publishDir "${params.data_dir}/contigs_bed", mode: 'copy', enabled: save_enabled

    container "tegreedy/rtools:2.0.0"

    input:
    path bed_file
    path cluster_table

    output:
    path "${bed_name}.annotated_filtered.bed", emit: re_bed_annot_ch

    script:
    bed_name = bed_file.baseName
    """
    Rscript "${r_script_path}"/mergeBedWithAnnotation.R ${bed_file} ${cluster_table}
    grep \"rDNA\\|CACTA\\|hAT\\|LINE\\|MuDR\\|plastid\\|satellite\" ${bed_name}.annotated.bed > ${bed_name}.annotated_filtered.bed
    """
}

process BED_TO_GFF {

    if (workflow.containerEngine == 'singularity' && !use_docker_container) {
        container "https://depot.galaxyproject.org/singularity/bioconductor-rtracklayer:1.52.0--r41hd029910_0"
    } else {
        container "quay.io/biocontainers/bioconductor-rtracklayer:1.52.0--r41hd029910_0"
    }

    input:
    path bed_file

    output:
    path "${bed_name}.gff", emit: re_gff_ch

    script:
    bed_name = bed_file.baseName
    """
    cat ${bed_file} | Rscript "${r_script_path}"/bed2gff.R ${bed_name}.gff
    """
}

process FILTER_GFF {
    publishDir "${params.data_dir}/annotations", mode: 'copy', enabled: save_enabled

    input:
    path gff_file

    output:
    path "${gff_file}", emit: re_gff_filtered_ch

    script:
    """
    perl -p -i -e \"s/name=/annot=/\" ${gff_file}
    """
}
