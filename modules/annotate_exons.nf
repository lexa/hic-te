#!/usr/bin/env nextflow

// Generated reference genome base name
ref_dir = file(params.reference).baseName

save_enabled = params.save_intermediate

process ANNOTATE_EXONS {

    publishDir "${params.data_dir}/annotations", mode: 'copy', enabled: save_enabled

    input:
    path exon_gff3_input

    output:
    path "${ref_dir}_exons.gff", emit: annot_exon_ch

    script:
    """
    grep \"exon\\|promoter\\|enhancer\" ${exon_gff3_input} | perl -p -e \"s/ID=/annot=exon;ID=/\" > ${ref_dir}_exons.gff
    """
}

