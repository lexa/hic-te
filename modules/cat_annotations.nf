#!/usr/bin/env nextflow

// Generated reference genome base name
ref_dir = file(params.reference).baseName

save_enabled = params.save_intermediate

process CAT_ANNOTATIONS {

    publishDir "${params.data_dir}/annotations", mode: 'copy', enabled: save_enabled

    input:
    path te_annot
    path re_annot
    path plantsat_annot
    path exon_annot

    output:
    path "${ref_dir}_fragments_annotated2.gff", emit: annot_all_ch

    script:
    """
    cat ${te_annot} ${re_annot} ${plantsat_annot} ${exon_annot} > ${ref_dir}_fragments_annotated2.gff
    """
}

process CAT_REPEAT_ANNOTATIONS {

    publishDir "${params.data_dir}/annotations", mode: 'copy', enabled: save_enabled

    input:
    path te_annot
    path re_annot
    path plantsat_annot

    output:
    path "${ref_dir}_repeat_fragments_annotated2.gff", emit: annot_repeat_all_ch

    script:
    """
    cat ${te_annot} ${re_annot} ${plantsat_annot} > ${ref_dir}_repeat_fragments_annotated2.gff
    """
}
