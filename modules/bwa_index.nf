#!/usr/bin/env nextflow

process BWA_INDEX {
    publishDir "${params.data_dir}", mode: 'copy', enabled: true

    input:
    path reference

    output:
    path "bwa_index"
    val "bwa_index/${reference}", emit: bwa_index_ch

    script:
    """
    mkdir bwa_index
    bwa index -p bwa_index/${reference} ${reference}
    """
}
