#!/usr/bin/env nextflow

use_docker_container = params.singularity_use_docker_container

process INTERSECT_BED {
    //Find overlap between bowtie2 mapping and TE-g-nester annotation for full-length TEs
    publishDir "${params.data_dir}/intersect_bed", mode: 'copy', enabled: params.save_intermediate

    if (workflow.containerEngine == 'singularity' && !use_docker_container) {
        container "https://depot.galaxyproject.org/singularity/bedtools:2.30.0--hc088bd4_0"
    } else {
        container "quay.io/biocontainers/bedtools:2.30.0--hc088bd4_0"
    }

    input:
    path bam_file
    path annotated_fragments_gff

    output:
    path "${bed_name}.bed", emit: bed_ch

    script:
    bed_name = "${params.sra_run}_" + file(annotated_fragments_gff).baseName + "_toHiC_All"
    """
    bedtools intersect -abam ${bam_file} -b ${annotated_fragments_gff} -wb -bed > ${bed_name}.bed
    """
}
